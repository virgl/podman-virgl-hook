# MIT License
#
# Copyright (c) 2023 Collabora Limited
#
# Author: Igor Ponomarev <igor.ponomarev@collabora.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

hook_generator = find_program('generate-oci-hook-json.py')

custom_target(
    'create-runtime-hook',
    command : [
        hook_generator,
        'createRuntime',
        get_option('prefix') / virgl_hook_libdir / virgl_hook_name
    ],
    output : 'virgl-create-runtime.json',
    install : true,
    install_dir : '/usr/share/containers/oci/hooks.d',
    capture : true,
)

custom_target(
    'poststop-hook',
    command : [
        hook_generator,
        'poststop',
        get_option('prefix') / virgl_hook_libdir / virgl_hook_name
    ],
    output : 'virgl-poststop.json',
    install : true,
    install_dir : '/usr/share/containers/oci/hooks.d',
    capture : true,
)
