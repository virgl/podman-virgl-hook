#!/usr/bin/env python3
# MIT License
#
# Copyright (c) 2023 Collabora Limited
#
# Author: Igor Ponomarev <igor.ponomarev@collabora.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from __future__ import annotations

from argparse import REMAINDER, ArgumentParser
from json import dump as json_dump
from sys import stdout


def generate_hook_dict(stage: str, commands: list[str]) -> dict:
    d = {"version": "1.0.0"}
    d["when"] = {"annotations": {"virgl": "enabled"}}
    d["stages"] = [stage]
    d["hook"] = {
        "path": "/usr/bin/python3",
        "args": ["/usr/bin/python3"] + commands + [stage],
        "timeout": 5,
    }

    return d


def main() -> None:
    parser = ArgumentParser()
    parser.add_argument(
        "stage",
        choices=("createRuntime", "poststop"),
    )
    parser.add_argument(
        "commands",
        nargs=REMAINDER,
    )

    hook_dict = generate_hook_dict(**vars(parser.parse_args()))

    json_dump(hook_dict, stdout, indent=4)


if __name__ == "__main__":
    main()
