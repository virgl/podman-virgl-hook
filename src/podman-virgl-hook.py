# MIT License
#
# Copyright (c) 2023 Collabora Limited
#
# Author: Igor Ponomarev <igor.ponomarev@collabora.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from __future__ import annotations

from argparse import ArgumentParser
from json import load as json_load
from os import kill, pipe, read
from pathlib import Path
from signal import SIGKILL
from subprocess import Popen
from sys import stdin

TMP_DIR = Path("/tmp/oci-virgl/")


def get_run_dir_and_pid_file(container_id: str) -> tuple[Path, Path]:
    container_dir = TMP_DIR / container_id
    pid_file = container_dir / "pid"
    return container_dir, pid_file


def post_stop(container_data: dict) -> None:
    container_id = container_data["id"]

    container_dir, pid_file = get_run_dir_and_pid_file(container_id)
    vtest_pid = pid_file.read_text()
    kill(int(vtest_pid), SIGKILL)
    pid_file.unlink()
    container_dir.rmdir()


def create_run_time(container_data: dict, vtest_executable: str) -> None:
    root = Path(container_data["root"])
    container_id = container_data["id"]

    read_fd, write_fd = pipe()

    proc = Popen(
        args=(
            vtest_executable,
            "--socket-path=./tmp/.virgl_test",
            "--ready-fd",
            str(write_fd),
            "--no-fork",
        ),
        pass_fds=(write_fd,),
        cwd=root,
    )

    read(read_fd, 1000)

    container_dir, pid_file = get_run_dir_and_pid_file(container_id)
    container_dir.mkdir(mode=0o700, parents=True)
    pid_file.write_text(str(proc.pid))


def run(stage: str, vtest_executable: str) -> None:
    container_data = json_load(stdin)

    if stage == "createRuntime":
        create_run_time(container_data, vtest_executable)
    elif stage == "poststop":
        post_stop(container_data)
    else:
        raise ValueError("Unknown oci hook stage:", stage)


def main() -> None:
    parser = ArgumentParser()
    parser.add_argument(
        "stage",
        choices=("createRuntime", "poststop"),
    )
    parser.add_argument(
        "--vtest-executable",
        default="/usr/bin/virgl_test_server",
    )

    run(**vars(parser.parse_args()))


if __name__ == "__main__":
    main()
